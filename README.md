# Mortgage sample calculations with iOS integration

This is a plain XCode project with no external dependencies. 
Simply Build, Run and see the Log for the calculated result.

## Contents

 - FormulatorBiundle: contains the Objective-C wrapper around the JavascriptCore interface accessing the calculations
 - Js: folder contains the generated JavaScript code doing the calculations
 


