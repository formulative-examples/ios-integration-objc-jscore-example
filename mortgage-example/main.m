//
//  main.m
//  mortgage-example
//
//  Created by Balázs Molnár on 2018. 10. 24..
//  Copyright © 2018. Balázs Molnár. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
