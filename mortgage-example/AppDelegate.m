//
//  AppDelegate.m
//  mortgage-example
//
//  Created by Balázs Molnár on 2018. 10. 24..
//  Copyright © 2018. Balázs Molnár. All rights reserved.
//

#import "AppDelegate.h"
#import "FormulatorBundle/MortgageCalculations.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    // initialize bundle (JSContext)
    MortgageCalculations *mortgageCalculationsInstance = [MortgageCalculations sharedInstance];
    
    // initialize input values
    NSMutableDictionary * input = [[NSMutableDictionary alloc] init];
    NSString * client_score_value = @"620";
    NSString * term_value = @"30";
    NSString * principal_value = @"200000";
    input[@(MORTGAGE_PMT_FROM_SCORE__PMT__CLIENT_SCORE)] = client_score_value;
    input[@(MORTGAGE_PMT_FROM_SCORE__PMT__TERM)] = term_value;
    input[@(MORTGAGE_PMT_FROM_SCORE__PMT__PRINCIPAL)] = principal_value;
    
    // call calculation
    NSDictionary * result = [mortgageCalculationsInstance doCalculation:MORTGAGE_PMT_FROM_SCORE__PMT
                                                              ofService:MORTGAGE_CALCULATIONS__MORTGAGE_PMT_FROM_SCORE
                                                              withInput:input];
    
    NSLog(@"calculation results:%@", result);
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
