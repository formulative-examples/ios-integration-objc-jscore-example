//
//  MortgageCalculations.m
//  version: 2018-10-24T07:15:59.887Z
//  Generated with Formulator
//

#import "MortgageCalculations.h"

@implementation MortgageCalculations
+ (id)sharedInstance {
  static MortgageCalculations *__sharedInstance;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    __sharedInstance = [[MortgageCalculations alloc] init];
  });
  return __sharedInstance;
}

- (id) init {
  self = [super init];
  self.jsContext = [[JSContext alloc] init];
  self.jsContext.exceptionHandler = ^(JSContext *context, JSValue *exception) {
    NSLog(@"JS Error: %@", exception);
  };
  [self.jsContext evaluateScript:@"var GLOBAL={};"];
  [self.jsContext evaluateScript:@"var LOCALEID=\"hu_hu\";"];
  NSError *err;
  NSString* path = [[NSBundle mainBundle] pathForResource : MORTGAGE_CALCULATIONS_BUNDLE_NAME
                                                   ofType : @"js"
                                              inDirectory : MORTGAGE_CALCULATIONS_BUNDLE_FOLDER];

  NSString * jsbundle = [NSString stringWithContentsOfFile : path
                                                  encoding : NSUTF8StringEncoding
                                                     error : &err];
  if (err) {
    NSLog(@"Error reading file: %@", err.localizedDescription);
    self = nil;
  } else {
    [self.jsContext evaluateScript:jsbundle];
  }
  return self;
}

- (NSDictionary *) formatInputDictionary: (NSDictionary *) inputDict {
  NSMutableDictionary * formattednputDict = [[NSMutableDictionary alloc] initWithCapacity: inputDict.count];

  for( NSNumber * key in inputDict) {
    NSString * newKey = [self formatInputKeysToString:key];
    [formattednputDict setObject : [inputDict objectForKey:key]
                          forKey : newKey];
  }
  return [[NSDictionary alloc] initWithDictionary:formattednputDict];
}

- (NSString*)formatInputKeysToString:(NSNumber *) inputNameAsNumber {
  NSString *result = nil;

  switch([inputNameAsNumber integerValue]) {
    case MORTGAGE_PMT_FROM_SCORE__PMT__CLIENT_SCORE: {
      result = @"clientScore";
      break;
    }
    case MORTGAGE_PMT_FROM_SCORE__PMT__TERM: {
      result = @"term";
      break;
    }
    case MORTGAGE_PMT_FROM_SCORE__PMT__PRINCIPAL: {
      result = @"principal";
      break;
    }
    case MORTGAGE_PMT_FROM_SCORE__PMT__MONTHLY_PAYMENT: {
      result = @"monthlyPayment";
      break;
    }
    default:
      [NSException raise:NSGenericException format:@"Unexpected InputType for mortgageCalculations calculation"];
  }

  return result;
}

- (NSDictionary *) doCalculation: (MortgageCalculationsCalculationNames) pCalculation
                       ofService: (MortgageCalculationsServiceNames) pService
                       withInput: (NSDictionary *) pInput {
  NSDictionary * formattednputDict;
  formattednputDict =[self formatInputDictionary:pInput];
  NSError *err;
  NSData *inputJsonValue = [NSJSONSerialization dataWithJSONObject : formattednputDict
                                                           options : NSJSONWritingPrettyPrinted
                                                             error : &err];
  if (err) {
    [NSException raise:NSGenericException format : @"Error converting calulcation input dictionary to JSON, error is: %@", err.localizedDescription];
  }

  NSString * inputJsonStr = [[NSString alloc] initWithData : inputJsonValue
                                                  encoding : NSUTF8StringEncoding];

  NSString * serviceName;
  NSString * calculationName;
  NSString * calculationCallString;

  switch(pService) {
    case MORTGAGE_CALCULATIONS__MORTGAGE_PMT_FROM_SCORE: {
      serviceName = @"mortgagePmtFromScore";
      break;
    }
    default: {
      NSString *msg = [NSString stringWithFormat:@"Unexpected Service parameter received by doCalculation (%u)", pCalculation];
      [NSException raise:NSGenericException format : @"%@",msg];
    }
  }

  switch(pCalculation) {
    case MORTGAGE_PMT_FROM_SCORE__PMT: {
      calculationName = @"pmt";
      break;
    }
    default: {
      NSString *msg = [NSString stringWithFormat:@"Unexpected Calculation parameter received by doCalculation (%u)", pCalculation];
      [NSException raise:NSGenericException format : @"%@",msg];
    }
  }

  calculationCallString = [NSString stringWithFormat:@"%@.%@.%@(%@)",
                           MORTGAGE_CALCULATIONS_BUNDLE_VARNAME,
                           serviceName,
                           calculationName,
                           inputJsonStr];

  NSLog(@"calc call string:%@", calculationCallString);

  NSDate *methodStart = [NSDate date];
  JSValue *aValue = [self.jsContext evaluateScript:calculationCallString];
  NSDate *methodFinish = [NSDate date];
  NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
  NSLog(@"Calculation executionTime (seconds) = %f", executionTime);
  NSDictionary *resultDict = [aValue toDictionary];
  return resultDict;
}


@end
