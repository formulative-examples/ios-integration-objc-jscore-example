//
//  MortgageCalculations.m
//  version: 2018-10-24T07:15:59.887Z
//  Generated with Formulator
//

#import <Foundation/Foundation.h>
#import <JavaScriptCore/JavaScriptCore.h>

static NSString * MORTGAGE_CALCULATIONS_BUNDLE_NAME=@"mortgageCalculations"; // filename
static NSString * MORTGAGE_CALCULATIONS_BUNDLE_FOLDER=@"Js";
static NSString * MORTGAGE_CALCULATIONS_BUNDLE_VARNAME=@"mortgageCalculations"; // exported object

// service names prefixed with bundle
typedef enum {
  MORTGAGE_CALCULATIONS__MORTGAGE_PMT_FROM_SCORE
} MortgageCalculationsServiceNames;

// calculation names - prefixed with service
typedef enum {
  MORTGAGE_PMT_FROM_SCORE__PMT
} MortgageCalculationsCalculationNames;

// input param names - prefixed with service
typedef enum {
  MORTGAGE_PMT_FROM_SCORE__PMT__CLIENT_SCORE,
  MORTGAGE_PMT_FROM_SCORE__PMT__TERM,
  MORTGAGE_PMT_FROM_SCORE__PMT__PRINCIPAL,
  MORTGAGE_PMT_FROM_SCORE__PMT__MONTHLY_PAYMENT
} MortgageCalculationsInputNames;


@interface MortgageCalculations : NSObject
@property (nonatomic, strong) JSContext * jsContext;

+ (id)sharedInstance;
- (NSDictionary *) doCalculation: (MortgageCalculationsCalculationNames) calculation
                       ofService: (MortgageCalculationsServiceNames) service
                       withInput: (NSDictionary *) input;
- (NSString*)formatInputKeysToString:(NSNumber *) inputNameAsNumber;


@end
